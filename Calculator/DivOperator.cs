using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Calculator
{
    class DivOperator : CalcAbstractOperator
    {
        public override double getResult(params double[] args)
        {
            return args[0] / args[1];
        }
    }
}