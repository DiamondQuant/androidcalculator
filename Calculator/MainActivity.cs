﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content.PM;

namespace Calculator
{
    [Activity(Label = "Calculator", MainLauncher = true, Icon = "@drawable/icon", 
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        private TextView textField;
        private double x;
        private double y;
        private double result;
        private CalcAbstractOperator op;
        private bool firstArgPassed = false;
        private bool secondArgPassed = false;
        private bool binOperatorPassed = false;
        private bool dotInserted = false;
        private bool resultShown = false;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.MainLinear);

            FindViewById<Button>(Resource.Id.button0).Click += On0Click;
            FindViewById<Button>(Resource.Id.button1).Click += On1Click;
            FindViewById<Button>(Resource.Id.button2).Click += On2Click;
            FindViewById<Button>(Resource.Id.button3).Click += On3Click;
            FindViewById<Button>(Resource.Id.button4).Click += On4Click;
            FindViewById<Button>(Resource.Id.button5).Click += On5Click;
            FindViewById<Button>(Resource.Id.button6).Click += On6Click;
            FindViewById<Button>(Resource.Id.button7).Click += On7Click;
            FindViewById<Button>(Resource.Id.button8).Click += On8Click;
            FindViewById<Button>(Resource.Id.button9).Click += On9Click;
            FindViewById<Button>(Resource.Id.buttonDot).Click += OnDotClick;
            FindViewById<Button>(Resource.Id.buttonAdd).Click += OnAddClick;
            FindViewById<Button>(Resource.Id.buttonSub).Click += OnSubClick;
            FindViewById<Button>(Resource.Id.buttonMult).Click += OnMultClick;
            FindViewById<Button>(Resource.Id.buttonDiv).Click += OnDivClick;
            FindViewById<Button>(Resource.Id.buttonSqrt).Click += OnSqrtClick;
            FindViewById<Button>(Resource.Id.buttonRes).Click += OnResClick;
            FindViewById<Button>(Resource.Id.buttonC).Click += OnCClick;

            textField = FindViewById<TextView>(Resource.Id.textField);
            textField.Text = "";
        }

        private void showErrorMessage(string text)
        {
            Toast toast = Toast.MakeText(this, text, ToastLength.Short);
            toast.Show();
        }

        private void clearFieldOnDemand()
        {
            if (binOperatorPassed && !secondArgPassed)
            {
                textField.Text = "";
                dotInserted = false;
                secondArgPassed = true;
            }
            else if (resultShown)
            {
                textField.Text = "";
                dotInserted = false;
                resultShown = false;
                firstArgPassed = true;
            }
            else if (!firstArgPassed)
            {
                firstArgPassed = true;
                dotInserted = false;
            }
        }

        private void On0Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "0";
        }

        private void On1Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "1";
        }

        private void On2Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "2";
        }

        private void On3Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "3";
        }

        private void On4Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "4";
        }

        private void On5Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "5";
        }

        private void On6Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "6";
        }

        private void On7Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "7";
        }

        private void On8Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "8";
        }

        private void On9Click(object sender, EventArgs e)
        {
            clearFieldOnDemand();
            textField.Text += "9";
        }

        private void OnDotClick(object sender, EventArgs e)
        {
            if (!firstArgPassed || binOperatorPassed && !secondArgPassed || resultShown)
            {
                showErrorMessage("Input some digits first.");
            }
            else if (dotInserted)
            {
                showErrorMessage("There can be only one dot.");
            }
            else
            {
                textField.Text += ".";
                dotInserted = true;
            }
        }

        private void OnAddClick(object sender, EventArgs e)
        {
            if (!firstArgPassed)
            {
                showErrorMessage("Pass an operand first.");
            }
            else if (binOperatorPassed)
            {
                showErrorMessage("An operator has been already passed.");
            }
            else
            {
                x = Convert.ToDouble(textField.Text);
                op = new AddOperator();
                binOperatorPassed = true;
                textField.Text += "" + '\n' + "+";
            }
        }

        private void OnSubClick(object sender, EventArgs e)
        {
            if (!firstArgPassed)
            {
                showErrorMessage("Pass an operand first.");
            }
            else if (binOperatorPassed)
            {
                showErrorMessage("An operator has been already passed.");
            }
            else
            {
                x = Convert.ToDouble(textField.Text);
                op = new SubOperator();
                binOperatorPassed = true;
                textField.Text += "" + '\n' + "-";
            }
        }

        private void OnMultClick(object sender, EventArgs e)
        {
            if (!firstArgPassed)
            {
                showErrorMessage("Pass an operand first.");
            }
            else if (binOperatorPassed)
            {
                showErrorMessage("An operator has been already passed.");
            }
            else
            {
                x = Convert.ToDouble(textField.Text);
                op = new MultOperator();
                binOperatorPassed = true;
                textField.Text += "" + '\n' + "*";
            }
        }

        private void OnDivClick(object sender, EventArgs e)
        {
            if (!firstArgPassed)
            {
                showErrorMessage("Pass an operand first.");
            }
            else if (binOperatorPassed)
            {
                showErrorMessage("An operator has been already passed.");
            }
            else
            {
                x = Convert.ToDouble(textField.Text);
                op = new DivOperator();
                binOperatorPassed = true;
                textField.Text += "" + '\n' + "÷";
            }
        }

        private void OnSqrtClick(object sender, EventArgs e)
        {
            if (!firstArgPassed)
            {
                showErrorMessage("Pass an operand first.");
            }
            else if (binOperatorPassed)
            {
                showErrorMessage("An operator has been already passed.");
            }
            else
            {
                if (x < 0)
                {
                    showErrorMessage("Operand can't be negative.");
                }
                else
                {
                    op = new SqrtOperator();
                    x = Convert.ToDouble(textField.Text);
                    result = op.getResult(x);
                    textField.Text = "Result: " + result.ToString("0.#####");
                    resultShown = true;
                    firstArgPassed = false;
                    secondArgPassed = false;
                }
            }
        }

        private void OnResClick(object sender, EventArgs e)
        {
            if (!secondArgPassed)
            {
                showErrorMessage("Pass two operands with a binary operator first");
            }
            else
            {
                y = Convert.ToDouble(textField.Text);
                result = op.getResult(x, y);
                textField.Text = "Result: " + result.ToString("0.#####");
                resultShown = true;
                firstArgPassed = false;
                secondArgPassed = false;
            }
        }

        private void OnCClick(object sender, EventArgs e)
        {
            textField.Text = "";
            firstArgPassed = false;
            secondArgPassed = false;
            binOperatorPassed = false;
            dotInserted = false;
            resultShown = false;
        }
    }
}

